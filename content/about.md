---
title: "Sobre mi"
date: 2022-05-30T02:12:56-05:00
draft: false
---

**Hispagatos Member**

- **Blog** https://hispagatos.org

- **Mastodon** https://hispagatos.space/@x3r0x

- **Correo** x3r0x@hispagatos.org

 **Cuentas**
 
- **sourcehut** https://sr.ht/~x3r0x/

**Donaciones:**
- **Donar usando Liberapay**
https://liberapay.com/x3r0x/donate
