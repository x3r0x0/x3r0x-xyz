---
title: "El hacking NO es un crimen"
date: 2021-09-24T18:07:43+02:00
draft: false
tags: ["hackers", "tecnologia", "cultura hacker", "hacking", "hacker ethics", "etica hacker"]
images: ["/images/hac kingisNOTacrime.png"]
---

### El Hacking NO es un crimen!!!
--- 

![El Hacking no es un crime logo](/images/hackingisNOTacrime.png)


Desde [**Hispagatos**](https://hispagatos.org/) , en países hispanohablantes y muchos otros grupos de la vieja y nueva escuela del mundo anglosajón llevamos diciendo décadas que el hacking es una cultura, no es una profesión, ni una rama técnica, ni un título, pero si una serie de acciones conmovidas por una personalidad, una manera de pensar crítica, muchas veces subversiva o rebelde que cuestiona todo, con la cual con llevar a la obsesión por entender y a veces cambiar todo lo que está roto, pero NO solo en la tecnología, al ser una personalidad, también lo refleja en otras facetas, desde la social, política y al funcionamiento de las cosas.

En países hispanohablantes aparte del problema del que hablamos abajo tenemos otro problema añadido al de los países Anglo Sajones, se denomina hacker al "pirata informático" cosa que es muy incorrecto, pero en otro blog hablaremos de eso más detenidamente, ya os lo hemos comentado muchas veces en Hackerñol.

Aquí voy a traducir unos trozos de texto sacados de la web que se ha creado en Ingles de [Hacking is NOT a crime](https://www.hackingisnotacrime.org/action).


El "hacking is not a crime", es una organización sin fines de lucro que aboga por la reforma de políticas globales para reconocer y salvaguardar los derechos de los hackers. Buscamos crear conciencia sobre el uso peyorativo de los términos "hacker" y "hacking" en los medios de comunicación y la cultura popular. Específicamente, el estereotipo negativo con el que tan a menudo se asocian los términos. Los Hackers a menudo son mal vistos y retratados como personas poco éticas con intenciones maliciosas.

Debido a esto, muchos Hackers se abstienen de revelar públicamente las vulnerabilidades de privacidad y seguridad que descubren por temor a represalias legales. En consecuencia, esto está creando una frontera digital cada vez más hostil para todos. El único remedio viable es brindar protección a los hackers que realizan investigaciones de buena fe sobre privacidad y seguridad entre otras.

Durante las últimas décadas, los medios de comunicación y la cultura popular mundiales han estado utilizando el término "Hacker" para describir a los entusiastas de la privacidad y la seguridad que cometen actos delictivos y no éticos. Esto es lamentable, pero comprensible. Dada la naturaleza esotérica de la privacidad y la seguridad, y el profundo impacto que ahora tienen en nuestra vida cotidiana, es fácil entender por qué nuestra persona está mal caracterizada.

Contrariamente a esta percepción errónea, ser un hacker es un estilo de vida, una cultura  y una mentalidad; una identidad. No es una declaración de moda o un personaje de película. Un hacker es simplemente un pensador ya ético de por sí, curioso e innovador que crea soluciones poco ortodoxas a problemas complejos. Las acciones y métodos mediante los cuales se resuelven estos problemas se denominan "hacks". Un delincuente se involucra en actividades poco éticas con intenciones maliciosas. Los hackers **NO** lo hacen.

Defendemos de que no es el individuo, su conocimiento o su profesión por la que la persona debe caracterizarse como ser un hacker. Más bien, son la personalidad, los medios, el motivo, la oportunidad y, lo que es más importante, la intención lo que importa. Por tanto, el Hacking no es ni puede ser un delito. Es simplemente un esfuerzo ético de exploración y resolución de problemas.

Y con esto pedimos que  cortésmente replicar a los que usan "hacker" en una connotación negativa en usar "ciber criminal" en su lugar. Dados que "Ciber" y "criminal" son los términos más correctos y con los que muchos ya están familiarizados, Las palabras de "Ciber Criminal" resuena bien con la mayoría de las personas. Por favor, usar el hashtag #HackingisNotacrime para crear conciencia.

Como hackers nos pica el arreglar todo y la palabra hacker esta rota por los medios de comunicación, la propaganda  y la gente que no entiende nuestra cultura.

No dejemos que la cultura y ética hacker se queden en incomprendido, algo mal dicho por ignorancia de unos y por a propósito de otros que lo quieren  manipular a una mera profesión o curriculum.
- [Post original](https://hispagatos.org/post/el_hacking_no_es_un_crimen/)
- ReK2
- gemini://rek2.hispagatos.org

Mas sobre el tema en:
- [Hackerñol Para Hackers en Español](https://odysee.com/@Hackernol:7)
- [Hacking is NOT a crime](https://www.hackingisnotacrime.org/action)
- [2600 Hacker Magazine](https://2600.com)
- [Off the hook - Hacker Radio show](https://2600.com/offthehook/)
- [Cult of the Dead Cow](https://cultdeadcow.com/)
- [Computer Chaos Club](https://www.ccc.de/en/)

Licencia:
- [Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/)
