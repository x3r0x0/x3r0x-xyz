---
title: "[Hacking on WhatsApp (Dumping SMS && Evading Malicious Permissions)] (POC)"
date: 2022-08-13
draft: false
tags: ["hackers", "hacking"]
---

Buenas noches, últimamente han estado llegando en forma de Spam/Apk's/Phishings maliciosas vía SMS, o enlaces con contenido malicioso pues he decidido hacer un vídeo para mostrar como realizan este tipo de ataques mediante el uso de Spyware/Malware para este tipo de fines. les recordamos que lo que realicen con lo que verán a continuación solo es responsabilidad del receptor ya que el vídeo está hecho con fines educativos realizados en el vídeo de manera manual y recordando que toda acción que hagan dependiendo de su país es totalmente punible y carcelable.
 
No hagan lo que no quieres que te hagan, usar herramientas de este tipo no tiene ningún tipo de mérito si planeas hacer algo malo con ellas y mucho mas usarlas sin tener conocimiento sobre hacking. No se estará usando Port Forwarding ni Ofuscadores para poder hacer lo mas digerible el vídeo y corto. en los próximos videos estaremos tocando la parte de los emuladores/contenedores "Dalvik/Smali" para analisis de malware, técnicas de ofuscación manual para evasión del Play Protect, Embebbed's APK, y ataques fuera de linea por Port Forwarding tanto como SSH o Tunel VPN sea desde un VPS o algún servicio de Port Forwarding existente además de técnicas que se usan de manera forense con las MSGDBCRYPT's y las MSGDBStores.
 
(Embebbed Services)
Recuerden que las aplicaciones pueden ser manipuladas e incluso pueden venir "infectadas" desde sitios legitimos. mientras instalas una App dentro de ella puede haber código malicioso mientras que piensas que es una aplicación legitima.
 
Airdroid -> Emulador del teléfono victima / ApkOnline -> Apk donde se "secuestrará" la sesión.
 
¿Qué haremos hoy?:
 
1- Generar una APK maliciosa la cual nos permita comprometer el dispositivo victima y correremos los servicios de Metasploit.
2- Descompilaremos nuestra APK para una breve explicación y entender que permisos otorga dentro del ManifestAndroid.xml de nuestro APK.
3- Buscaremos la ruta de los MSGDBStores con las copias de seguridad MSGDBCRYPT para ver que copias de seguridad se han hecho (esto se realiza con la finalidad de saber si se podrán leer esas copias una vez secuestrada la sesión, si no las hay todos los chats pueden aparecer en blanco o simplemente los grupos de WhatsApp donde la persona está)
4- Deautenticar y Reautenticar la sesión de WhatsApp que se desea comprometer.
5- Obtener la autenticación via SMS generada por WhatsApp, agregarla en la cuenta que se desea
6- Voilá.
 
Preguntas frecuentes: 
 
¿Qué necesitamos? 
 
-Dos emuladores, uno para observar que es lo que ocurre del lado del infectado y con el otro emulador será donde se secuestrará la sesión.
 
¿Hay mas maneras de "hackear" WhatsApp?
 
R/: Sí, las hay. esta es una de las maneras generales que se realizan para poder acceder a una sesión de WhatsApp.
 
¿Qué otras maneras hay de "hackear" un WhatsApp?
 
Todo dependerá a que va enfocado el vector de ataque o la técnica forense que se realice al dispositivo. 
1- Existe la escalación de privilegios en dispositivos Android/iOS a root para la obtención de las MSGDBSTORES/DataStores (Copias de seguridad) alojadas entre los contenedores Dalvik y las rutas de las carpetas de WhatsApp (Orientado a pruebas forenses)
2- SS7 attack basado en Bypass a redes GSM desde Hookers IMSI (IMSI identify and catching) obteniendo la paquetería SMS y todo lo que pase por esa red GSM 
3- Man in The Middle Attack (MITM) + Ingeniería Social + Phishing desde un Portal Cautivo para suplantación y robo de credenciales.
4- Smishing Attack + Asterisk Providers generando números aleatorios para envío de código malicioso, o supuestas promociones.
5- Disclosure Session o el mal llamado "Banneo", esto se hace pidiendo rescate a WhatsApp reportando como robado el dispositivo o por Spam. 
6- SIM Swapping consiste en llamar al proveedor de telefonia/red GSM con tus datos (Documento de Identidad) para duplicar el número telefónico de la víctima y robar todos sus datos.
 
¿Qué puedo hacer para evitar que me hackeen WhatsApp/Facebook/Etc?
1- No descargues nada ni abras enlaces de terceros, si es necesario usa virustotal.com o cualquier identificador online de spyware.
2- Activa la verificación a dos pasos de WhatsApp para generar un PIN extra ya que este no puede ser obtenido debido a que pasa por la misma App de WhatsApp y no en texto plano como un SMS (también aplica en otras apps con doble
factor de autenticación)
3- No confiar tu información ni aceptar peticiones para validar otro correo o número telefónico sea de un amigo, familiar o un desconocido ya que usan esto para robarte las credenciales.
4- No conectarte a Redes Wi-Fi Gratuitas
5- Evitar dar tu número telefónico a extraños ya que existen métodos diferentes para poder obtener información
6- Usar algún autenticador extra, puede ser el de Google para poder proteger tus aplicaciones importantes.
7- No envíes fotos de tus documentos o datos que permitan la suplantación de tu información y tu vida privada.
 
 # Ahora si pueden ver el Video:
 
 https://peertube.nz/w/2jwBs7xV8PA4Pkqy8oGcmp
 
-----------------------------------------------------------
(Sources de Alternativas para instalación de emuladores)  |
-BlueStacks						                                    |	
                                                          |
https://www.bluestacks.com/es/index.html                  |
                                                          |
-Airdroid                                                 |
https://www.airdroid.com                                  |
-----------------------------------------------------------

- Lyk0s
- FoxShell Team
