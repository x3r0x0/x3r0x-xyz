---
title: "New HOPE"
date: 2022-07-16
draft: false
author: "x3r0x"
tags: ["tech", "tecnologia", "hacking", "hackers", "privacy"]
images: [/images/newhope_logo.png]
---

## a New HOPE July 22-24 Evento de la cultura hacker en NY

![Imagen publicitaria](/images/newhope_logo.png)

A New HOPE será una conferencia transformadora para la comunidad hacker, de muchas maneras. Todos hemos pasado por mucho, y ha sido un desafío. 
Es un momento para volver a unirnos para inspirar, transformar y compartir ESPERANZA. A New HOPE será en vivo y en persona, y en un gran lugar nuevo. 
St. John's University tiene más espacio, más posibilidades y nos ofrece mucho más apoyo para HOPE en el futuro. Echaremos de menos nuestra antigua casa, 
el Hotel Pennsylvania, que, lamentablemente, está siendo demolido. Pero ahora estamos listos para nuestro próximo capítulo. Esperamos que se una a nosotros
en esta parte emocionante de la historia mientras creamos juntos Una Nueva ESPERANZA.

No olvides reservar tu acceso aqui:

https://mobilizon.public.cat/events/d7a781db-d35a-4955-bb32-1ad7b57ca44c

Ya puedes participar remotamente desde el barrio #cyberpunk de #hispagatos al próximo HOPE. Aún está en construcción así que tengan un poco de paciencia.  

AQUI: https://hubs.mozilla.com/zWXK8U6/hope2022
