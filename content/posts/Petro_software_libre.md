---
title: "Gustavo Petro y las tecnologías emergentes."
date: 2022-06-09
draft: false
author: "x3r0x"
tags: ["tech", "tecnologia", "softwarelibre"]
images: ["https://www.x3r0x.xyz/images/GustavoPetro2.jpg"]
---


Me resulta curiosa la propuesta de Petro para impulsar el uso del software libre, las tecnologías emergentes y colaborativas. 

![Imagen publicitaria](/images/GustavoPetro2.jpg)


Eso me recuerda los años en que Chávez daba discursos sobre independencia tecnológica y el uso del software libre para todas las instituciones públicas del país, para así evitar el saboteó y paralización por parte de las grandes empresas tecnológicas gringas como lo hicieron en 2002. Es cuando sale a la luz la Resolución 025 publicada en la [Gaceta Oficial N° 39.633 de fecha lunes 14 de marzo de 2011](https://www.cnti.gob.ve/images/stories/documentos_pdf/gaceta_oficial_%2039.633.pdf) donde se establece el uso de Canaima GNU/Linux como sistema operativo de las estaciones de trabajo de los organismos y entes de la Administración Pública Nacional de la República Bolivariana de Venezuela, con el objetivo de homogeneizar y fortalecer la plataforma tecnológica del Estado venezolano, dando cumplimiento al marco legal vigente. 

![Frase](/images/Chavez2.jpeg)

Todo se hizo con el sueño de poder cumplir con los objetivos del Plan de la Patria,lamentablemente pudimos ver qué todo esto fue tomado con mucho escepticismo y resistencia en la mayoría de instituciones públicas de Venezuela, tambien por parte de muchos jovenes los cuales vieron mucha limitacion de estos equipos a la hora de poder jugar, todo esto dando como resultado que aún hoy haya muchas instituciones/jovenes que usan versiones desfasadas de Windows [Microsoft dejo de dar soporte a Windows 7 el 14 de enero de 2020](https://support.microsoft.com/es-es/windows/el-soporte-de-windows-7-finaliz%C3%B3-el-14-de-enero-de-2020-b75d4580-2cc7-895a-2c9c-1466d9a53962) haciéndolos muy vulnerables a ciberataques. 

### Politica invisible de Gustavo Petro durante su alcaldia

Durante su alcaldia en Bogotá Petro impulsó un plan de gobierno denominado [Plan de Desarrollo
Económico, Social, Ambiental y de Obras Públicas para Bogotá D.C. 2012-2016 Bogotá
Humana](http://www.idep.edu.co/sites/default/files/Plan_Desarrollo_Bogota_Humana_2012-2016.pdf) donde buscaba promover la utilización del software libre en el Distrito Capital. Todo esto fue muy bien visto por parte de la comunidad de sofware libre, ya que anteriormente este mismo habia promovido el software libre, durante la administración Petro fue posible la inclusión de miembros de la comunidad software libre como asesores o funcionarios de las secretarías relacionadas con el tema tecnológicos.

Cuando Petro llegó a la alcaldía se presentó en el [Flisol del año 2012](https://www.asle.ec/exposicion-de-alcalde-de-bogota-en-flisol-2012/) afirmando ser promotor y defensor del software libre 
y comprometiéndose a difundirlo y a hacer uso del acuerdo 297 de 2007 para lograr que Bogotá fuera pionera en el tema, sin embargo, esto nunca se cumplió. Parece que Petro delegó las funciones 
relativas a la tecnología a la Alta Consejería de TIC, entidad creada con propósitos de aplicar la reglamentación respectiva.
En ese entonces la cabeza de las TIC, dirigida por Mauricio Trujillo Uribe, no era cercano al software libre y los alcances de la política tuvieron como base el trabajo de algunas personas interesadas en el tema 
que actuaron de manera desarticulada desde distintas secretarías del gobierno distrital. De tal modo que la política distrital que afirmó el cumplimiento del Acuerdo 297 de 2009 no logró sus propósitos.

En 2013 la revista ENTER,hizo una publicacion en donde afirmaban que "Bogotá está en medio de una migración hacia Linux y software libre", mencionando el caso de la migración al sistema operativo Linux de 992 computadores personales del distrito, bajo los términos del acuerdo 297, lo anterior en aplicación de las políticas distritales de Petro. El ahorro de licencias privativas se estimó en unos 280 millones de pesos. Evidentemente todo esto fue criticado fuertemente por parte de los relacionistas públicos de empresas como Microsoft cuyos intereses con el Estado Colombiano son inmensos. Argumentando que 
"La migración al software libre llevaría como consecuencia inmediata el requerimiento de expertos en el tema, técnicos con conocimiento en la programación y administración de sistemas Linux que reemplazarían a sus similares expertos en herramientas de software propietario. Donde supuestamente los nuevos expertos llegarían a ocupar diversos cargos en el sector público dominado por el paradigma previo. Lo más relevante, en todo caso, es la posibilidad de que muchos miembros de la comunidad de software libre logren un tipo de reconocimiento social que conllevaría a la consecución de puestos de trabajo y salarios del sector público. Es cuando los defensores del software libre afirman que "más allá de las ventajas económicas en relación con el ahorro en el costo de las licencias, los beneficios colectivos se desprenden de la filosofía misma del software libre." 

Pese a ese pequeño rayo de luz mencionado anteriormente, este proceso fue bastante tímido, si comparamos la inversion entre el software privativo y el libre. En el año 2013 la alcaldia realizó inversiones en Linux por valor de 58 millones de pesos, ahora bien si nos vamos a lo privativo tenemos que en ORACLE (una plataforma privativa especializada en Bases de Datos) se hizo una inversion de 8.900 Millones de Pesos, por ejemplo en licencias de Suite Informátiva Privativa para la Contraloría de Bogotá fue de 655.692.168 Millones de pesos.

Es entonces que la ausencia de apoyo político por parte del gobierno nacional/distrital influyó en la desmotivación del uso software libre, ya que habian muchas expectativas en que la administración Petro en la Alcaldía de Bogotá, se abriera paso a la implementación de software libre de manera masiva en el Distrito. Esto, sin embargo no ocurrió.

Aunque se debe reconocer que se dieron algunos avances,en la propuesta de software libre como alternativa en Bogotá, como es el caso de algunos proyectos complementados en algunas entidades como el [IDRD](https://www.idrd.gov.co/) y tambien el gran esfuerzo para tratar de implementar las directrices del [acuerdo 279 de 2007](https://tic.bogota.gov.co/sites/default/files/documentos/Acuerdo%20279%20de%202007_SoftwareLIbre.pdf) .

### El libre mercado estatal

La no aprobación de una ley de software libre en Colombia aportó a una cierta desarticulación del movimiento software libre. Esto puede reafirmarse si ponemos el ejemplo la desaparición de Colibrí una comunidad de usuarios de software libre de Colombia la cual impulsaba proyectos de software libre.

Todo este fracaso en las leyes nacionales se da especialmente por culpa de poderosas multinacionales fabricantes de software privativo presentes en Colombia, como Microsoft, Oracle o SAP. Empresas que tienen capacidad de ejercer presión política y han sido activamente adversas, por ejemplo, a los proyectos de ley de software libre que se han intentado y fracasado en el país”.  Puede ser esta una de las razones por las el movimiento del software libre se haya roto, o bien, que muchos de los miembros del movimiento software libre hayan migrado hacia otros proyectos diferentes en un intento de promover el uso del sistema operativo Linux en el estado.

#### Una pequeña esperanza...

Con el crecimiento de la oposición en el senado, la posible victoria de Petro a la presidencia, esperemos que se pueda lograr aprobar una legislación que permita un crecimiento del software libre desarrollado para el sector público, también motivar el uso de GNU/Linux en las instituciones publicas y sacar de ese letargo profundo en el que esta el movimiento del software libre desde hace 10 años. 

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="<iframe width="1280" height="720" src="https://www.youtube.com/embed/iuVUzg6x2yo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>" frameborder="0" allowfullscreen></iframe>
