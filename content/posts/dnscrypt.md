---
title: "COMO CONFIGURAR DNSCRYPT-PROXY"
date: 2023-05-15
draft: false
tags: ["hackers", "tecnologia", "cultura hacker", "hacking"]
---

Aquí hay un tutorial rápido sobre [dnscrypt-proxy](https://github.com/DNSCrypt/dnscrypt-proxy) y cómo configurarlo :)

Bienvenido a un tutorial paso a paso de dnscrypt-proxy. Esto fue probado con Arch Linux, *meow*.
La mayor parte de esto funcionará con otras distros de Linux pero aquí usamos Arch.

### ¿Qué es?

La [página wiki de Arch](https://wiki.archlinux.org/index.php/Dnscrypt-proxy) dice que es "un proxy DNS con soporte para los protocolos DNS encriptados DNS sobre HTTPS
y DNSCrypt, que pueden ser usados para prevenir ataques man-in-the-middle y ataques eavesdropping. dnscrypt-proxy es también compatible con DNSSEC".

### ¿Por qué usarlo?

DNS es un servicio que todo el mundo utiliza, pero transmite todo en texto plano. Esto permite usar DNS de forma segura con encriptación para que ni siquiera tu ISP 
pueda ver tus peticiones DNS :) También puedes tunelizar peticiones a través de servidores DNSSEC conocidos.

No SÓLO obtienes cifrado DNS, sino que si se configura correctamente con [Anonymized-DNS](https://github.com/DNSCrypt/dnscrypt-proxy/wiki/Anonymized-DNS) puede evitar 
que los intermediarios graben y manipulen el tráfico DNS, ya que "evita que los servidores sepan nada acerca de las direcciones IP de los clientes, mediante el uso de 
relés intermedios dedicados a reenviar datos DNS cifrados". Esto se tratará en este tutorial.

También puedes configurar [Filters](https://github.com/DNSCrypt/dnscrypt-proxy/wiki/Filters) a través de listas negras, listas negras de IPs y listas blancas, las cuales serán discutidas brevemente en este tutorial.

### Instalación y configuración

Esta configuración usará dnscrypt-proxy sin IPv6 y sin DNS-sobre-https, pero siéntase libre de cambiar a sus necesidades y lo animo a experimentar.

1) Primero instalalo:

```
	$ sudo pacman -S dnscrypt-proxy
```

2) A continuación lo configuramos de la siguiente manera:

```
	$ sudo cp /etc/dnscrypt-proxy/dnscrypt-proxy.toml /etc/dnscrypt-proxy/dnscrypt-proxy.toml.ORIGIN
	$ sudo vim /etc/dnscrypt-proxy/dnscrypt-proxy.toml
```

* Ahora tienes que especificar los [server_names](https://github.com/DNSCrypt/dnscrypt-resolvers) que estén orientados a la privacidad:

```
	server_names = [dnscrypt.eu-dk', 'dnscrypt.eu-nl', 'dnscrypt.uk-ipv4', 'ffmuc.net', 'meganerd', 'publicarray-au', 'scaleway-ams', 'scaleway-fr', 'v.dnscrypt.uk-ipv4']
```

* Tambien es importante determinar los criterios de los servidores utilizados, esto puede cambiar dependiendo de tus necesidades:

```
	ipv4_servers = true
	ipv6_servers = false		# No uso ipv6
	dnscrypt_servers = true
	doh_servers = false		# Desactivo DoH porque no uso ningún servidor con él

	require_dnssec = true
	require_nolog = true
	require_nofilter = true
```

* * Establecer una respuesta para consultas bloqueadas:

```
	blocked_query_response = 'refused'
```

* Establece el registro en syslog:

```
	use_syslog = true
```

* (Opcional) crear una clave nueva y única para cada consulta DNS: [RTFM]

```
	dnscrypt_ephemeral_keys = true
```

* Escoge una dirección [privacy-oriented fallback and netprobe address] Aquí usamos DNS sin censura, que está en [esta lista de "DNS de privacidad"](https://waluszko.net/2018/04/10/pricacy-oriented-dns-providers-review/), 
pero puedes cambiar a lo que quieras:


```
	fallback_resolvers = ['91.239.100.100:53']
	netprobe_address = '91.239.100.100:53'
```

* Puedes bloquear el ipv6 aquí también:

```
	block_ipv6 = true
```

* Puedes tambien ampliar la lista de "implementaciones rotas" para evitar ciertos servidores:

```
	fragments_blocked = ['cisco', 'cisco-ipv6', 'cisco-familyshield', 'cisco-familyshield-ipv6', 'quad9-dnscrypt-ip4-filter-alt', 'quad9-dnscrypt-ip4-filter-pri', 'quad9-dnscrypt-ip4-nofilter-alt', 'quad9-dnscrypt-ip4-nofilter-pri', 'quad9-dnscrypt-ip6-filter-alt', 'quad9-dnscrypt-ip6-filter-pri', 'quad9-dnscrypt-ip6-nofilter-alt', 'quad9-dnscrypt-ip6-nofilter-pri', 'cleanbrowsing-adult', 'cleanbrowsing-family-ipv6', 'cleanbrowsing-family', 'cleanbrowsing-security']
```

* A continuación, configure las rutas DNS anónimas - [más información aquí](https://github.com/DNSCrypt/dnscrypt-proxy/wiki/Anonymized-DNS) y la [lista de relays DNS anónimos aquí](https://github.com/DNSCrypt/dnscrypt-resolvers/blob/cf4421044283813e31d26b6ccc8f02759b993f33/v3/relays.md):

```
	routes = [
    { server_name='dnscrypt.eu-dk', via=['anon-meganerd', 'anon-scaleway-ams'] },
    { server_name='dnscrypt.eu-nl', via=['anon-meganerd', 'anon-scaleway-ams'] },
    { server_name='dnscrypt.uk-ipv4', via=['anon-scaleway', 'anon-tiarap'] },
    { server_name='ffmuc.net', via=['anon-ibksturm', 'anon-scaleway-ams'] },
    { server_name='meganerd', via=['anon-scaleway', 'anon-tiarap'] },
    { server_name='publicarray-au', via=['anon-ibksturm', 'anon-tiarap'] },
    { server_name='scaleway-ams', via=['anon-scaleway', 'anon-meganerd'] },
    { server_name='scaleway-fr', via=['anon-meganerd', 'anon-v.dnscrypt.uk-ipv4'] },
    { server_name='v.dnscrypt.uk-ipv4', via=['anon-scaleway', 'anon-meganerd'] }
    ]

```

* luego guardar configuración y salir

3) A continuación, configure una lista negra (opcional):

```
	$ sudo vim /etc/dnscrypt-proxy/dnscrypt-proxy.toml
```

*  En la sección [blocked_names], puedes descomentarla y usar una lista de dominios publica o tener la tuya propia:

```
	 blocked_names_file = '/etc/dnscrypt-proxy/blocked-names.txt'
```

* Crea tu propio archivo de lista negra a partir del [ejemplo](https://github.com/DNSCrypt/dnscrypt-proxy/wiki/Filters) y [utiliza esta referencia](https://github.com/DNSCrypt/dnscrypt-proxy/wiki/Public-blocklist).
También existen otras listas negras. 

* También puede descomentar 'log_file' en esta sección si desea un registro de lo que está en la lista negra. También puede descomentar 'log_file' en esta sección si 
desea un registro de lo que está en la lista negra. Esto es muy útil para solucionar problemas de su blacklist.txt.

4) A continuación, configure una lista blanca (opcional):

```
	$ sudo vim /etc/dnscrypt-proxy/dnscrypt-proxy.toml
```

* En la sección [allowed_names], descomente y establezca:

```
   allowed_names_file = '/etc/dnscrypt-proxy/allowed-names.txt'

```

* Cree su propio archivo de lista blanca a partir del [ejemplo](https://github.com/DNSCrypt/dnscrypt-proxy/blob/master/dnscrypt-proxy/example-allowed-names.txt).

* Además, puede descomentar 'log_file' en esta sección si desea un registro de lo que está en la lista blanca.

5) Te animo a jugar con la lista negra de IPs, [ejemplo aquí](https://github.com/DNSCrypt/dnscrypt-proxy/blob/master/dnscrypt-proxy/example-blocked-ips.txt)

6) Cambia /etc/resolv.conf para usar dnscrypt-proxy:

```
	$ sudo vim /etc/resolv.conf
	nameserver 127.0.0.1		# Asegúrese de que este es su nameserver
```

### Vamos a iniciarlo y hacer pruebas.
    
Ejecute dnscrypt-proxy:

```
	$ sudo systemctl start dnscrypt-proxy.service
```

Compruebe si ahora se ejecuta en el puerto 53

```
    $ ss -lp 'sport = :domain
```

Ahora prueba dnscrypt-proxy:

```
	$ sudo pkill -STOP dnscrypt-proxy
```
	
A continuación, intente acceder a un sitio web y si no puede entonces su tráfico DNS está pasando con éxito a través del proxy, por lo que lo volvemos a activar:

```
	$ sudo pkill -CONT dnscrypt-proxy
```

Prueba opcional, vaya a [dnsleaktest.com](dnsleaktest.com), haga clic en prueba extendida, y luego verifique que funciona ya que los resultados no mostrarán los DNS 
de su ISP sino sólo los de nuestro archivo de configuración.

### Opción para iniciar dnscrypt-proxy en el arranque:

Si quieres, configúralo para que se cargue al arrancar:

```
	$ sudo systemctl enable dnscrypt-proxy.service
```

***
### Solución de problemas:

P1: ¿Qué pasa si /etc/resolv.conf se sobrescribe al reiniciar?

R1: Sigue las instrucciones de arriba para cambiar pero también usa el siguiente comando después para asegurarte de que resolv.conf no cambia:

```
chattr +i /etc/resolv.conf
```

***
#### Información para leer:

(1) [dnscrypt-proxy source code](https://github.com/DNSCrypt/dnscrypt-proxy)

(2) [Wiki de Arch Linux sobre dnscrypt-proxy](https://wiki.archlinux.org/index.php/Dnscrypt-proxy)

(3) [Instalación de dnscrypt-proxy en Linux](https://github.com/DNSCrypt/dnscrypt-proxy/wiki/Installation-linux) - de los creadores

***
### Cualquier otra cosa...

Cambia lo que necesites y disfruta :) Si hay algo más que me perdí entonces hágamelo saber. ¡Como siempre -> RTFM, hackea el sistema y disfruta de la vida! 
- x3r0x
- [x3r0x en Mastodon](https://hispagatos.space/@x3r0x)
