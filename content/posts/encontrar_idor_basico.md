---
title: "Insecure direct object reference"
date: 2023-02-11
draft: false
tags: ["hackers", "hacking"]
---
## IDOR es un tipo de vulnerabilidad de control de acceso.
### Definicion:
Este tipo de vulnerabilidad puede ocurrir cuando un servidor web recibe una entrada proporcionada por el usuario para recuperar objetos (archivos, datos, documentos), se ha depositado demasiada confianza en los datos de entrada y no se valida en el lado del servidor para confirmar la objeto solicitado pertenece al usuario que lo solicita.

## Como encontrar IDOR's?
- un ejemplo clasico de IDOR es cuando por ejemplo en el enlace de abajo cambiamos el **user_id** nuestro por ejemplo el **1600** por el de otro
```
http://pagina-vulnerable.es/profile?user_id=1600
```
- si esto ocurre y el software no verifica el **input** del usuario es muy posible que nos muestre informacion privada de otro usuario con lo cual es una vulnerabilidad simple pero muy severa.
### Por medio de pasar ID's codificados
- por ejemplo en base64
```
echo {'id':'1200'} | base64
> e2lkOjEyMDB9Cg==
```
aqui tenemos **e2lkOjEyMDB9Cg==**  por lo que si vemos esto podemos copiarlo y decodificarlo, y modificarlo
```
> echo e2lkOjEyMDB9Cg== | base64 -d 
> {id:1200}
> echo {'id':'1201'} | base64
> e2lkOjEyMDF9Cg==
```
- normalmente todo esto se hace por medio de interceptar el POST/GET request con herramientas de proxy como [ZAProxy](https://www.zaproxy.org/)
### Por medio de una identificacion hash
- Esta forma es un poco mas complicada, especialmente si siguen un patrón predecible
- en MD5 el ID 123 se convertiria en 202cb962ac59075b964b07152d234b70
- siempre es recomendable antes de nada probar a poner el hash en [CrackStation](https://crackstation.net/) o jugar con el en [cyberChef](https://gchq.github.io/CyberChef/)
### Por el metodo del cambio (swap)
- Cuando no se puede detectar con ninguno de los metodos mencionados arriba se puede uno crear dos cuentas separadas y mirar las diferencias entre ellas
- si estando logeado con una cuenta, puedes cambiar el ID a la otra y ver la informacion, ya sabes que es vulnerable, un simple bruteforce puede sacar informacion de varias cuentas.
- Yo recomiendo siempre hacer 4 cuentas, sacar los id's en el formato en que esten y mirar las diferencias y jugar con ellas en [cyberChef](https://gchq.github.io/CyberChef/)
# Donde podemos encontrar IDORs?
- No siempre vamos a encontrar este tipo de enlaces en URL's a simple vista en la barra del navegador etc.
- Es muy posible que sea una llamada de AJAX o una refenrencia en un fichero .js de javascript.
- Hay veces que los desarolladores se les olvida borrar referencias que podemos explotar
- por ejemplo:
```
/usuario/detalles
```
- Esto nos mostraria nuestra informacion, pero no vemos a simple vista nuestro ID, ni el parametro etc.
- por lo que nos toca hacer un poco de **mineria de parametros** 
- Algunos de estos parametros que podemos tratar son:
- ?ID=
- ?id=
- ?user=
- ?username=
- ?profile=
- ?user_id=
```
/usuario/detalles?id=1600
```
- aqui vemos que si nos da un resultado, asi que hemos conseguido encontrar un IDOR.
- Recomendable crear un programa en RUST/GO/Bash etc para tratar diferentes combinaciones y posibilidades
- Uno de los trucos mejor guardados de un hacker es la capazidad de crear diccionarios simples pero efectivos
- no queremos tener un diccionario de 50K palabras basura, cuando estamos buscando por algo que solo tiene unas 40 combinaciones
- como es el caso de los parametros que casi siempre son muy parecidos o iguales en todos los frameworks, por lo que un diccionario
- grande nos va a joder mas que ayudar, nos va a blockear el WAF ( web application firewall ), vamos a hacer mucho ruido y vamos a tardar mucho mas. 

## Un ultimo ejemplo mas completo
- Vamos a una pagina web que use AJAX esto de abajo es solo de ejemplo, no sirve ya que usamos una pagina estatica ;)
```
https://hispagatos.org/customers
```
- esto nos mostraria la informacion nuestra, **no** la de todos los usuarios, solo la nuestra.
- a simple vista no vemos nada verdad? nada de id= ni user= ni nada de nada...
- pues ahora viene la magia, abre en el navegador las herramientas de desarollo y ves a la zona "network" o "red" y **haz un reload** o con tu web proxy como ZAP vas y lo mismo un reload.
- Fijate en las llamadas al servidor, y si cambiamos a otra parte de nuestro profile por ejempl "Your account" que le forzamos a mostrar mas informacion sobre nosotros
```
https://hispagatos.org/customers/account
```
- Y ahora **SI** vemos una llamada del tipo **JSON** que pone debajo de **File**
```
customer?id=1200
```
- Como veis hemos encontrado la informacion que queriamos y que no estaba a simple vista.
- si vamos clickeamos en la entrada a la llamada **json** en la parte de la red en las herramientas de desarollo nos manda a algo como:
```
https://hispagatos.org/api/v2/customer?id=1200
```
- aqui ya podemos cambia el **id=** y si nos muestra la informacion de otro usuario ya tenemos el **IDOR**