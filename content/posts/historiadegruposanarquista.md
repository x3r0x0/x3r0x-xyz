---
title: "Hackea el sistema! Los grupos anarquistas de hacking"
date: 2023-03-17
draft: false
author: "x3r0x"
tags: ["tech", "tecnologia", "hacking", "hackers",]
---


## Cult of the Dead Cow

El Cult of the Dead Cow es uno de los grupos anarquistas de hacking más antiguos y reconocidos. Este grupo se enfoca en la liberación de información y el acceso a la tecnología. Desarrollaron herramientas como [Back Orifice](https://web.archive.org/web/20180715070715/http://www.cultdeadcow.com/tools/bo.html), que permitía a los usuarios tomar control de una computadora  a través de una conexión TCP/IP
Con esta herramienta, el Cult of the Dead Cow demostró la vulnerabilidad de los sistemas Windows 95 y Windows 98 a los ataques de tipo rootkit. Además, el grupo también promovió la cultura de la ética hacker y el uso responsable de la tecnología.

## Electronic Disturbance Theater

Fundado en 1998. Este grupo se enfocaba en la lucha contra la globalización y la defensa de los derechos humanos. Desarrollaron una herramienta llamada [FloodNet](https://anthology.rhizome.org/floodnet), que permitía a los usuarios hacer ataques de denegación de servicio contra sitios web corporativos. Con esta herramienta, el Electronic Disturbance Theater demostró la capacidad de la gente común para hacer frente al poder corporativo.

## Chaos Computer Club

El Chaos Computer Club (CCC) es uno de los grupos de hackers más influyentes en el mundo. Desde su fundación en 1981 en Berlín Oeste por Wau Holland y un grupo de amigos, el CCC se ha caracterizado por su activismo político y social y por su compromiso con la defensa de la privacidad y la libertad en la era digital.

En sus inicios, el CCC se centró en dar acceso a los ordenadores a la mayor cantidad posible de personas, con el objetivo de democratizar el acceso a la tecnología. Esto se lograba a través de la organización de talleres y cursos en los que se enseñaba a programar y a utilizar herramientas informáticas. De esta forma, el CCC contribuyó a la creación de una comunidad de hackers y activistas digitales comprometidos con la defensa de los derechos civiles y políticos en la era digital.

Sin embargo, el CCC no se limitó a la divulgación de la tecnología. Desde sus primeros años, el CCC se caracterizó por su compromiso con la denuncia de casos de inseguridad informática y su defensa de la privacidad y la libertad de expresión en Internet. En este sentido, el CCC ha participado en numerosas acciones de activismo político y social, que han contribuido a generar conciencia sobre los peligros de la vigilancia y la censura en la era digital.

Entre los casos más destacados en los que el CCC ha participado se encuentra la denuncia de la vulnerabilidad en la red BTX, una red de servicios en línea utilizada en Alemania en la década de 1980 y 1990. El CCC demostró que era posible acceder a los datos de los usuarios de esta red sin necesidad de una autorización previa, lo que llevó a la toma de medidas de seguridad por parte de los operadores de la red.

Otro caso en el que el CCC participó activamente fue la denuncia de la tecnología ActiveX de Microsoft. El CCC demostró que esta tecnología permitía a los desarrolladores de software acceder a los datos de los usuarios de Internet Explorer sin su consentimiento, lo que generó una gran polémica en la época.

En cuanto a la clonación de tarjetas GSM, el CCC fue uno de los primeros grupos en demostrar la vulnerabilidad de esta tecnología. En 1998, el CCC publicó un informe en el que explicaba cómo era posible clonar tarjetas SIM de teléfonos móviles, lo que llevó a un importante debate público sobre la seguridad de las comunicaciones móviles.

Además de estas acciones de activismo político y social, el CCC ha llevado a cabo numerosas investigaciones y denuncias de casos de espionaje y vigilancia por parte de gobiernos y empresas privadas. En este sentido, el CCC ha sido fundamental para generar conciencia sobre la necesidad de proteger la privacidad y la libertad de expresión en la era digital.

## Anonymous

Fundado en 2003 como un movimiento descentralizado sin líderes ni estructuras fijas. Este grupo se enfoca en protestar contra las injusticias sociales y políticas mediante operaciones masivas de hacking o activismo digital. Algunas de sus acciones más famosas son: hackear sitios web gubernamentales o corporativos, filtrar documentos secretos o confidenciales, apoyar movimientos populares como el los ataques a la [cienciologia](https://web.archive.org/web/20080213094432/http://www.computerworld.com.au/index.php/id%3B632197333) o el [Occupy Wall Street](https://es.wikipedia.org/wiki/Occupy_Wall_Street) , entre otras. Anonymous también ha hackeado o expuesto a los infosecs. Un caso famoso fue el de Hector Monsegur, alias Sabu, el líder de LulzSec que se convirtió en informante del FBI después de ser arrestado en 2011. Anonymous reveló su identidad y la de otros miembros de LulzSec, y los acusó de colaborar con las autoridades para detener a otros hackers. 

## LulzSec

Fundado en 2011 como una escisión de Anonymous. Este grupo se enfocaba en hackear sitios web por diversión o por provocación (lulz), algunas de sus acciones más notorias son: hackear sitios web como Sony Pictures, CIA, FBI, PBS, entre otros; publicar datos personales o financieros de miles de usuarios; declararse en guerra con otros grupos hackers; entre otras. También  se burlaban de los que se dedicaban a la ciberseguridad llamándolos "incompetentes" y "fracasados". También se burlaba de las autoridades que intentaban capturarlos, como el FBI y la Agencia contra el Crimen Organizado del Reino Unido (SOCA), a los que llegaron a tumbar sus sitios web. LulzSec también hacía bromas telefónicas a las empresas que atacaba, publicando sus números de contacto y animando a sus seguidores a llamarlos.

## BBS

Las BBS eran sistemas de comunicación que permitían a los usuarios conectarse a través de módems y compartir información, archivos, programas y mensajes sobre temas relacionados con el hacking, el phreaking, la anarquía y el carding. Estas BBS eran parte de la cultura hacker underground, que se basaba en la ética hacker de Steven Levy y las ideas anarquistas de liberación de información y acceso a la tecnología. Estas BBS eran un espacio de expresión, aprendizaje y resistencia para los hackers anarquistas, que buscaban cuestionar el sistema establecido y crear una sociedad más libre y horizontal.

## Telecomix 

Surgió en 2009 como una resistencia a las leyes de control que se imponían desde el Parlamento Europeo y desde entonces ha participado en diversas acciones para subvertir los derechos digitales y apoyar a los movimientos emancipatorios. Durante la revolución egipcia del 2011 ayudaron en la creación de líneas telefónicas autónomas, cuando el gobierno cortó el acceso a internet. Telecomix proporcionó números de teléfono gratuitos y seguros para que la gente pudiera comunicarse y difundir información. También enviaron faxes con noticias e instrucciones para usar el módem dial-up. [También interceptaron el tráfico web sirio y lo redirigió a una página con consejos para usar redes privadas virtuales (VPN), servidores proxy y software de anonimato como Tor.](https://web.archive.org/web/20110925103708/http://reflets.info/opsyria-when-the-internet-does-not-let-citizens-down/) También ayudaron a los activistas sirios a documentar y denunciar las violaciones de derechos humanos cometidas por el régimen. Tambien hicieron publicos unos registros que denunciaban la vigilancia masiva en Siria por parte de la empresa Bluecoat. [Telecomix obtuvo y analizó 54 GB de logs que revelaban que el gobierno sirio usaba dispositivos Bluecoat para filtrar, bloquear y espiar el tráfico web de los ciudadanos.](https://web.archive.org/web/20111007013638/http://tcxsyria.ceops.eu/95191b161149135ba7bf6936e01bc3bb)

### Si estás interesado en aprender más sobre los grupos anarquistas de hacking, aquí te dejamos algunos enlaces de interés:

[An Anarchist Explains How Hackers Could Cause Global Chaos](https://www.npr.org/sections/alltechconsidered/2018/02/26/583682587/how-hackers-could-cause-global-chaos-an-anarchist-explains)
Esta es una entrevista con Barrett Brown, un ex miembro de Anonymous que cumplió cárcel por hackear una firma privada para revelar secretos.
Hacktivism - Wikipedia2: Esta es una página que explica qué es el hacktivismo o hactivismo (un término que combina hack y activismo), cómo surgió y cuáles son sus objetivos.

[Anarchisthackers](https://www.noisebridge.net/wiki/Anarchisthackers/)
Esta es una página que habla sobre los hackers anarquistas, su participación en el movimiento Occupy y su visión del mundo.

[CultoftheDeadCow](https://cultdeadcow.com/)
Esta es la pagina de Cult of the dead cow la cual contiene contenido sobre hacking, activismo y cultura. El grupo publica medios como textos, audios, videos y software en su blog

[Chaos Computer Club](https://www.ccc.de/)
Este es el sitio oficial del CCC donde pueden encontrar información detallada sobre la historia y las actividades del CCC, así como noticias y eventos relacionados con el hacking, la privacidad y sus eventos.

[La Zona Autónoma Temporal, Anarquía Ontológica, Terrorismo Poético](https://hermetic.com/bey/taz_cont)
Este libro explora la idea de crear lo que llama "Zonas Temporalmente Autónomas" (TAZ), espacios temporales y autónomos que funcionan fuera del control del Estado y de las estructuras sociales establecidas.También argumenta que la tecnología y la cultura popular pueden ser utilizadas para crear estas zonas, que funcionan como espacios de resistencia contra la opresión y la dominación. Además, el libro explora la idea de la "anarquía ontológica", que se refiere a la liberación personal y el rechazo de las estructuras sociales y políticas dominantes.

[BBS](http://www.textfiles.com/bbs/)
Aquí puedes encontrar archivos sobre cómo funcionaban los BBS, qué redes y comunidades se formaban entre los usuarios, o cómo eran algunos BBS famosos o históricos.

[Telecomix](https://web.archive.org/web/20111229225258/https://telecomix.org/)

- x3r0x
- [x3r0x en Mastodon](https://hispagatos.space/@x3r0x)