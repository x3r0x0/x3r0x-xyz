---
title: "Ya es hora"
date: 2021-02-03T16:38:06+01:00
draft: false
author: "x3r0x"
tags: ["tech", "tecnologia", "hacking", "hackers", "anarquismo", "anarchism", "hacktivism", "social justice", "Justicia social", "privacidad", "privacy"]
images: ["https://hispagatos.org/images/Bandera_Hacker_Anarquista.jpg"]
---

### Compañeros, ya es hora de ponerse las pilas...

![Bandera del Hacker Anarquista](/images/Bandera_Hacker_Anarquista.jpg)
---
En nuestro mundo de habla hispana, tenemos gente haciendo un trabajo fenomenal, tanto en Latinoamérica como en Iberia, pero nos hace falta mucho pero mucho más... Hay que ponerse ya las pilas.

**Atentos** que esto **NO es una critica** a la vieja guardia Anarquista, sino un llamamiento a levantar cabeza y mirar hacia delante, no hacia atrás, salir de nuestras cuatro paredes y nuestra zona de confort. Tampoco me voy a adentrar mucho en tópicos de lucha particulares como wikileaks, Anon Anarchist Action, antisec, leftsec etc... Bueno, aquí vamos.


* Para poner un poco de contexto al tema. Desde mediados de los 80's influyentes compañeros Anarquistas y hackers, o hackers anarquistas o anarco hackers -o como se le dé la gana a cada uno llamarlo ya que hay muchos hackers anarquistas que no se ponen la pegata de "anarquista"- se tomaron en serio y pronosticaban el nuevo mundo digital del siglo XXI, mientras otros aliados nos daban la espalda porque nos gustaba la tecnología; ya entonces la veíamos como una arma necesaria porque sabíamos que el capital los usaría en contra de la gente a su provecho en maneras que pocos podían visionar. Proyectos con ideales anarquistas, como [la descentralizacion, la libertad, la claridad, el conocimiento libre, gratuito y abierto, y la desconfianza a toda autoridad](http://project.cyberpunk.ru/idb/hacker_ethics.html) se pasaron al mundo digital en formato de movimientos como el del [software libre](https://es.wikipedia.org/wiki/Software_libre), u otros más radicales y de acción directa como el ["warez"](https://es.wikipedia.org/wiki/Warez), o expropiatorio como el ["carding"](https://en.wikipedia.org/wiki/Carding_(fraud)), o el [robo digital de bancos y fortunas de la clase alta](https://packetstormsecurity.com/files/155392/HackBack-A-DIY-Guide-To-Rob-Banks.html) -no quiero entrar aquí en sus debates de que si nos sirven o no sirven, pero han pasado, de eso no hay duda- y  ciertamente, por desgracia, hoy en día tambien usados por cyber-mafias egocéntricas, cyber-criminales y gente avariciosa, mas que la idea original del hacker y hacker anarquista de hacerlo por ideologías políticas, subversión, protesta o razones éticas y morales.

* En las puertas del siglo XXI en 1995 al 1999 y las protestas contra el WTO de 1999 que paralizaron la ciudad de Seattle, fue en el movimiento anti-globalizacion donde en el mundo Anglosajón tomó mucha fuerza el hacking y formas digitales como nuevas formas de lucha, notablemente de aquí salieron compañeros y grupos descentralizados como los de [indymedia](https://es.wikipedia.org/wiki/Independent_Media_Center) y luego el [colectivo de riseup](https://riseup.net/es/about-us), solo por decir unos... Era la necesidad de hackers de volver a tomar el control de nuestra informacion que, desde la entrada del capital a Internet a principios de los 90's, estaban ya censurando y tapando la internet utópica y descentralizada; nuestros foros libres donde nos organizabamos sin autoridades que nos censuraran cualquier idea contra de los intereses del capital. También notablemente [la guerra de hackers contra el estado y las empresas](https://en.wikipedia.org/wiki/The_Hacker_Crackdown) que se dio en esta década, igualmente notable [la guerra de los cripto anarquistas](https://en.wikipedia.org/wiki/Crypto_Wars) con los estados que querían controlar los medios encriptados creados por hackers anarquistas donde salimos venciendo en esta primera batalla...


* Y para no aburrir más, ahora vamos al 2015, las teorías de los hackers de los 80's desgraciadamente han venido a ser realidad, estamos viviendo sin que la gente lo sepa en una distopia mundial, donde [el capitalismo de la vigilancia](https://en.wikipedia.org/wiki/Surveillance_capitalism), la guerra abierta en todos los campos contra las fuerzas subversivas a este nuevo orden económico con sede en wall street y silicon valley, están arrasando a su paso; por medio de propaganda han podido captar a millones de personas a usar internet de forma centralizada en sus plataformas o granjas de datos, los compañeros de la vieja guardia que ya en su día nos criticaban por usar tecnología, se movieron a usar Internet, pero no nos escucharon y se movieron directamente a estas plataformas, con el pretexto de que así podían llevar a más gente su mensaje, lo que no querían saber es que lo que hacían es promover y beneficiar al estado y al [capitalismo de la vigilancia](https://en.wikipedia.org/wiki/Surveillance_capitalism), mientras están en facebook hablando de la concentración en calle tal a tal hora, el capital ha ganado miles de Dolares solo con esos datos y lo peor echando de por tierra 20 años de trabajo de los anarquistas en la red creando herramientas libres y encriptadas, foros donde poder hablar libremente y formas de poder comunicarse sin miedo a ser arrestados o encarcelados por nuestras ideas.

* En todo este tiempo poco a poco más compañeros, sobre todo del mundo Anglosajón, han creado un movimiento enorme basado en teorías de hoy. Alrededor del mundo de hoy contando con las luchas sociales nuevas, y se han ganado muchas batallas. Movimientos que usando las herramientas creadas por hackers han podido "igualmente" llevar el mensaje a las masas de jóvenes, usando métodos viejos como panfletos, el boca a boca, quedadas y demás usando e-mail encriptado con [gnupg](https://gnupg.org/), pancartas, [telefonos con roms alternativos y encriptados](https://www.makeuseof.com/best-custom-android-roms/),  y demás. No les hacía falta Facebook ni Instagram ni WhatsApp, y cuando se han usado estas tecnologías maléficas del capital y neoliberalismo del siglo XXI, ha sido esporádico y puntual para promover la organización en otras plataformas libres y promovidas por anarquistas, nunca para quedarse y acomodarse. En la mayoría de casos se escriben bots para promover la información o desinformación según convenga en estas redes centralizadas de las autoridades autoritarias de silicon valley.


* Algunos grupos como ["IT's Going Down"](https://itsgoingdown.org/) aun así siguieron en estas plataformas, pero últimamente se han despertado, a la fuerza, pues Facebook, Twitter y demás les han tirado con escusas baratas. Por fin en el mundo Anglosajón el Anarquismo está ahora más o menos con el mismo punto de mira y luchando en todas las formas igualmente, tanto en la calle como antes y tácticas de antes como también en las redes con tácticas y acción directa nuevas al mismo tiempo que promoviendo herramientas y plataformas 100% libres y descentralizadas. No telegram NO es libre y mucho menos WhatsApp.

Fin del contexto.

--- 
## Aquí es donde entra mi reflexión sobre lo que nos ocurre en Iberia y países Latinoamericanos.

Mientras todo lo dicho arriba ha pasado, es desgraciadamente en la mayor parte y esquivando algunos grupos e individuos aquí y allá, solo en el mundo Anglosajón.

Para mí reflexión pongo [de ejemplo el articulo](https://www.mic.com/p/inside-kolektiva-the-social-media-platform-built-by-anarchists-activists-42617028) 
[https://www.mic.com/p/inside-kolektiva-the-social-media-platform-built-by-anarchists-activists-42617028](https://www.mic.com/p/inside-kolektiva-the-social-media-platform-built-by-anarchists-activists-42617028)

En dicho articulo los anarquistas de hoy en el mundo Anglosajón están promoviendo plataformas libres como [mastodon](https://joinmastodon.org/), [peertube](https://joinpeertube.org/) y demás, poniendo de ejemplo la revolución española de 1936.


> Lopez explains, “The idea of federations is one that has been embraced by anarchists for many decades, most notably during the Spanish Civil War.” (During the Spanish Civil War, millions of people organized their lives in a bottom-up fashion through federations without a centralized state.) “The fact that we can run our own Mastodon server and never be kicked out, like what recently happened to [anarchist news site] It's Going Down and others on Facebook, and still be part of this larger constellation of Mastodon servers, or Fediverse, is a very anarchist vision of what the world should look like. Small self-governing communities or affinity groups, united but decentralized.”


Lo que es la ironía y decimos esto con todo respeto ya que conocemos que hay una barrera linguistica de medios: en España hoy en día los anarquistas vivimos en el siglo XX y los que llegaron nuevos a las redes sin leer lo que los compañeros expertos en luchas sociales en redes, escribían en Inglés, seguimos pensando en Facebook, WhatsApp etc como herramientas donde influir en las masas positivamente, y esto puede tener en momentos puntuales y de forma puntual realmente un uso, pero siempre de forma temporal y promoviendo alternativas. También, pensamos en la lucha de clases de forma tradicional, pero el mundo ya no es tradicional.


Vamos cuando hablo en plural no significa todos, que si hay 4-5 gatos, que yo conozco de Barcelona, y otros sitios de Américalatina e Iberia, que sí que han avanzado y no se quedaron en la vieja guardia.

Pero vamos la mayoría hoy están usando Facebook, WhatsApp, Instagram, etc como si fueran inocentes, lo que por ignorancia o testarudos no se dan cuenta de que es peor usar WhatsApp que comprar un coche Mercedes. Ayuda más al estado y al capital el petroleo negro, la informacion; las riquezas hoy son tus datos, no tu dinero. Los ricos no quieren tu dinero, lo que quieren son tus datos para así vender eso... Es la mano invisible, sin darte cuenta eres como el que en 1990 tenia una camiseta de Buenaventura Durruti, creada por El Corte ingles; hoy eres el que muestra sus ideas por medio de Facebook que es peor que tener una camiseta de Durruti creada por El Corte Inglés. Tus datos tienen más riqueza que cualquier dinero que gastes, es la forma más indirecta de hacerse más ricos sin que la gente sé de ni cuenta que son vacas lecheras en cada mensaje que mandan y cada post de Instagram, de cada foto y panfleto de protesta que suben a estas redes, están dando leche a la mano invisible [del capitalismo de la vigilancia](https://en.wikipedia.org/wiki/Surveillance_capitalism).


Para que compañeros de la vieja guardia nos entiendan, si tienes problemas o te quieres poner un lavabo nuevo, ¿qué haces? ¿te lo montas tú a tu manera mal y con fallos o vas al compañero anarquista que es fontanero y le pides a el consejo para que te ayude? Pues hoy en día hay muchos [hackers anarquistas](https://www.noisebridge.net/wiki/Anarchisthackers/), pedirnos consejos, leer nuestros escritos y mirar nuestras herramientas así nos va mejor a todos en todas las formas de lucha.

Entonces, ¿qual creemos que ha sido el problema?,  ¿por que estamos desconectados?, ¿como podemos cambiar esto?

Lo que ha pasado:

Pues sinceramente es una mezcla de varias cosas y otras que no sabremos nunca.
- Grupos haciendo su propia movida, des-coordinación, etc.
- Desinformación de medios del capital, ya sean películas extranjeras, revistas y demás que nos ciegan a lo que pasa de verdad en el mundo.
- la falta de colaboración con grupos "internacionales" en luchas actuales a nivel mundial.
- La comodidad de vivir del pasado, Iberia fue un fuerte ejemplo de lucha anarco
 sindicalista y mirando tanto al pasado no vemos el presente.
- otros movimientos más progresistas(suaves), troskistas
(de vanguardia, ir donde la gente y moverlos a toda costa) o Marxistas leninistas (comunismo de estado, totalitario, verticalidad ).  Han infectado los valores libertarios e internacionalistas "horizontales" y parece ser que hemos perdido la independencia del "hazlo tu mismo", "crear nuestro mundo, nuestros espacios, si tambien espacios digitales que sean **libres y seguros**", etc.

Que se puede hacer?

- Conectar con grupos de hackers anarquistas internacionales y grupos de anarquistas de todo el globo y colaborar, no tenemos bandera ni frontera, ¿pues a que esperamos?
- no podemos solo ver los medios independientes de iberia o Américalatina, hay que salir de la caja, aprender ingles, privacidad en las redes, a programar y si puede ser mucho más de lleno, a hackear no me refiero a seguridad Informatica sino a controlar los medios digitales, no copiar, pero crear, conocer mucho mas que los ingenieros, saber romper, amar lo que haces y revolucionar los nuevos medios del poder, ante todo sabiduría.
- El pasado es respetable y hay que saber respetarlo y dar gracias y analizar, pero eso no nos puede impedir ver lo que pasa delante de nuestros ojos y leer, escuchar teorías y reflexiones nuevas e incomodas.
- Tenemos que ser independientes, pensar en lo nuevo y como utilizarlo para el cambio social sin perder nuestras convicciones, ya en 1936 no fue casualidad que la CNT tuviera tanto empeño en organizar la telefónica...¿Por que? era el medio de comunicación de aquel presente, y como hoy, quien controla la información controla muchas cosas...

Y corto el rollo...
Quiero decir que todo esto es mi más sincera posición que puede cambiar en cualquier momento y solo lo escribimos con ánimos de crear debate y mejorar nuestro activismo en todos nuestros frentes, dentro y fuera de las redes digitales.

Ánimo a todos los que se consideran en favor de cualquier modo de anarquismo de ponerse al día
Con libros actualizados del capitalismo, como la reflexión sobre el capitalismo de vigilancia y otros relacionados con la acción directa en formato digital y redes que han realizado compañeros [anarco hackers](https://www.noisebridge.net/wiki/Anarchisthackers/) los últimos 30 años.

Documentales sobre Hacker Anarquistas como ["hackea el sistema"](https://kolektiva.media/videos/watch/f0c80ab8-aa46-49d4-b662-5c7b6060a2d4)

Busca en tu librería local o pide libros como ["El capitalismo de vigilancia"](https://www.casadellibro.com/libro-la-era-del-capitalismo-de-la-vigilancia/9788449336935/11667796), 
un libro que dice:

>    «Como otra obra maestra reciente del análisis económico, El capital en el siglo XXI, de Thomas Piketty, este libro cuestiona ideas asumidas, plantea interrogantes incómodos sobre el presente y el futuro, y acota el terreno para un debate muy necesario y pendiente desde hace ya un tiempo.»
    Nicholas Carr, autor de Superficiales, Los Angeles Review of Books
    En esta obra magistral por la originalidad de las ideas y las investigaciones en ella expuestas, Shoshana Zuboff nos revela el alarmante fenómeno que ella misma ha denominado «capitalismo de la vigilancia». Está en juego algo de la máxima importancia: toda una arquitectura global de modificación de la conducta amenaza con transfigurar la naturaleza humana misma en el siglo XXI de igual modo a como el capitalismo industrial desfiguró el mundo natural en el siglo XX.
    Gracias al análisis de Zuboff, cobran gráficamente vida para nosotros las consecuencias del avance del capitalismo de la vigilancia desde su foco de origen en Silicon Valley hacia todos los sectores de la economía. Hoy se acumula un enorme volumen de riqueza y poder en unos llamados «mercados de futuros conductuales» en los que se compran y se venden predicciones sobre nuestro comportamiento, y hasta la producción de bienes y servicios se supedita a un nuevo «medio de modificación de la conducta».
    La amenaza que se cierne sobre nosotros no es ya la de un Estado «Gran Hermano» totalitario, sino la de una arquitectura digital omnipresente: un «Gran Otro» que opera en función de los intereses del capital de la vigilancia. El exhaustivo y turbador análisis de Zuboff pone al descubierto las amenazas a las que se enfrenta la sociedad del siglo XXI: una «colmena» controlada y totalmente interconectada que nos seduce con la promesa de lograr certezas absolutas a cambio del máximo lucro posible para sus promotores, y todo a costa de la democracia, la libertad y nuestro futuro como seres humanos.
    Sin apenas resistencia en la legislación o en la sociedad, el capitalismo de la vigilancia va camino de dominar el orden social y determinar el futuro digital... si no se lo impedimos antes.

desgraciadamente como decía a Iberia no nos llega mucha literatura o noticias sobre estas cosas... Pero si tenemos grupos y redes alternativas donde esta información se mueve mucho... Por favor ya es hora. Ponerse al día, no es solo ir donde está la gente, es también vivir con el ejemplo ante todo! crea el mundo que quieres ver, usa las herramientas menos malas, [GNU/linux](https://es.wikipedia.org/wiki/GNU/Linux) ante MS-windows, [Signal](https://signal.org/en/), no es descentralizado pero si abierto y de un [compañero anarquista, Signal](https://www.wired.com/2016/07/meet-moxie-marlinspike-anarchist-bringing-encryption-us/) o mejor [matrix](https://matrix.org/) que si es descentralizado ante WhatsApp o Telegram. Y vivir dando ejemplo, eso se empieza con uno mismo y nunca nadie dijo que seria fácil, ya que dura toda una vida de aprender, errores, correguir, borrar y empezar ir poco a poco cambiandose a uno mismo.

Para más información y detalles ya que esto hay información para horas y horas, ponerse en contacto con grupos de hackers anarquistas, sea cual sea este grupo, todos están dispuestos a ayudar a compañeros que organizan en otros tipos de lucha y quieren entender y poder conseguir sus objetivos en el mundo de hoy, de una manera educada, concienciada y lo mas importante, "**segura**".

#### Una reflexion por:
- ReK2
- [ReK2 en Mastodon @rek2@hispagatos.space](https://hispagatos.space/@rek2)
- x3r0x
- [x3r0x en Mastodon](https://hispagatos.space/@x3r0x)
- [Hispagatos](https://hispagatos.org)
- [Hispagatos en Gemini](gemini://blog.hispagatos.org)
- [Hispagatos en Mastodon @colectivo@hispagatos.space](https://hispagatos.space/@colectivo)


# Mastodon Anarquista
- [Kolektiva.social](https://kolektiva.social/about)
- [hispagatos.space](https://hispagatos.space)
- [anarchism.space](https://anarchism.space/about)

# PeerTube videos Anarquista
- [Kolektiva.media](https://kolektiva.media)

# Lemmy Anarquista
- [161.social - nodo lemmy para anti-autoritarios](https://lemmy.161.social/)

# Fediverso general
- [Fediverse Party](https://fediverse.party/)

# Consejos generales de privacidad
- [Privacy Tools](https://privacytools.io/)

# Matrix
- [Clientes del protocolo de charla Matrix](https://matrix.org/docs/projects/try-matrix-now/)

# Telefonos libres
- [Pine Phone](https://www.pine64.org/pinephone/)
- [librem 5](https://puri.sm/products/librem-5/)

# Roms de Android libres de Google
- [LineageOS](https://wiki.lineageos.org/)
- [Cualquier android rom libre que no tenga GAPPS y con MicroG](https://www.androidpolice.com/2020/06/09/install-use-custom-rom-no-google-apps/)
